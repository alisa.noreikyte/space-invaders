/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import java.io.IOException;

/**
 *
 * @author Alisa
 */
public class SpaceInvaders {

    static int MAP[][] = {
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,},};

    public static void main(String[] args) throws IOException {

        int character = 0;
        int laserCannonX = 14;
        int laserCannonY = 20;
        int weaponX = laserCannonX;
        int weaponY = laserCannonY - 1;
        boolean ifX = false;

        int alienX1 = 19;
        int alienY1 = 6;
        int aWeaponX1 = alienX1;
        int aWeaponY1 = alienY1 + 1;

        int alienX2 = 26;
        int alienY2 = 6;
        int aWeaponX2 = alienX2;
        int aWeaponY2 = alienY2 + 1;

        int alienX3 = 13;
        int alienY3 = 6;

        do {
            for (int y = 0; y < MAP.length; y++) {
                for (int x = 0; x < MAP[0].length; x++) {
                    int element = MAP[y][x];
                    if (laserCannonX == x && laserCannonY == y) {
                        System.out.print("W");
                    } else if (element == 4) {
                        System.out.print(" ");
                    } else if ((alienX1 == x && alienY1 == y) || (alienX2 == x && alienY2 == y) || (alienX3 == x && alienY3 == y)) {
                        System.out.print("Y");
                    } else if (weaponX == x && weaponY == y && character == 120) {
                        System.out.print("*");
                    } else if ((aWeaponX1 == x && aWeaponY1 == y) || (aWeaponX2 == x && aWeaponY2 == y)) {
                        System.out.print("@");
                    } else if (element == 0) {
                        System.out.print(" ");
                    } else if (element == 3) {
                        System.out.print(" ");
                    } else {
                        System.out.print("#");
                    }
                }
                System.out.println("");
            }

            if ((aWeaponX1 == laserCannonX && aWeaponY1 == laserCannonY) || (aWeaponX2 == laserCannonX && aWeaponY2 == laserCannonY)) {
                System.out.println("GAME OVER");
                System.exit(0);
            }

            if (aWeaponY1 >= weaponY && aWeaponX1 == weaponX) {
                aWeaponY1 = alienY1 + 1;
                weaponY = laserCannonY - 1;
            } else if (MAP[aWeaponY1 + 1][aWeaponX1] == 0) {
                aWeaponY1 = aWeaponY1 + 1;
            } else if (MAP[aWeaponY1 + 1][aWeaponX1] == 1) {
                aWeaponY1 = alienY1 + 1;
            }

            if (aWeaponY2 >= weaponY && aWeaponX2 == weaponX) {
                aWeaponY2 = alienY2 + 1;
                weaponY = laserCannonY - 1;
            } else if (MAP[aWeaponY2 + 1][aWeaponX2] == 0 || MAP[aWeaponY2 + 1][aWeaponX2] == 3) {
                aWeaponY2 = aWeaponY2 + 1;
            } else if (MAP[aWeaponY2 + 1][aWeaponX2] == 1) {
                aWeaponX2 = alienX2;
                aWeaponY2 = alienY2 + 1;
            } else if (MAP[aWeaponY2 + 1][aWeaponX2] == 2) {
                MAP[aWeaponY2 + 1][aWeaponX2] = 3;
                aWeaponY2 = alienY2 + 1;
            }

            do {
                character = System.in.read();
            } while (character == '\n' || character == '\r');

            switch (character) {
                case 'a':
                    if (MAP[laserCannonY][laserCannonX - 1] == 0) {
                        laserCannonX = laserCannonX - 1;
                        weaponX = weaponX - 1;
                    }
                    break;
                case 'd':
                    if (MAP[laserCannonY][laserCannonX + 1] == 0) {
                        laserCannonX = laserCannonX + 1;
                        weaponX = weaponX + 1;
                    }
                    break;
                case 'x':
                    weaponY = weaponY - 1;
                    if (MAP[weaponY + 1][weaponX] == 2) {
                        MAP[weaponY + 1][weaponX] = 3;
                        weaponY = laserCannonY - 1;
                    } else if (MAP[0][weaponX] == MAP[weaponY][weaponX]) {
                        weaponY = laserCannonY - 1;
                    } else if (alienY3 == weaponY && alienX3 == weaponX) {
                        MAP[alienY3][alienX3] = 4;
                        weaponY = laserCannonY - 1;
                        System.out.println("ONE ALIEN IS KILLED");
                    }
                    break;
            }
        } while (character != 'q');

    }

}
